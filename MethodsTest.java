public class MethodsTest {
	public static void main(String[] Args) {
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x + 50);
		methodTwoInputNoReturn(5, 10.5);
		int storedVal;
		storedVal = methodNoInputReturnInt();
		System.out.println(storedVal);
		System.out.println(sumSquareRoot(6, 3));
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}

	public static void methodNoInputNoReturn() {
		int x = 50;
		System.out.println("I’m in a method that takes no input and returns nothing");
	}

	public static void methodOneInputNoReturn(int x) {
		System.out.println("Inside the method one input no return " + x);
	}

	public static void methodTwoInputNoReturn(int y, double z) {
		System.out.println(" method with 2 inputs");
		System.out.println(y);
		System.out.println(z);
	}

	public static int methodNoInputReturnInt() {

		return 6;
	}

	public static double sumSquareRoot(int var1, int var2) {
		double sqrt;
		double varSum = var1 + var2;
		sqrt = Math.sqrt(varSum);
		return sqrt;
	}
}
